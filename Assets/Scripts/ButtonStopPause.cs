using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonStopPause : MonoBehaviour
{
    public Text text;
    public GameObject canvas;
    private bool stopped = true;
    
    public void OnClick()
    {
        stopped = !stopped;
        canvas.SetActive(stopped);
        text.text = stopped ? "Resume" : "Pause";
        if (stopped)        AudioManagerClips.AudioManager.AudioSnapMenu.TransitionTo(0.1f);
        else        AudioManagerClips.AudioManager.AudioSnapGame.TransitionTo(0.1f);

    }
}
