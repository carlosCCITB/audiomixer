using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class AudioManagerClips : MonoBehaviour
{
    public static AudioManagerClips AudioManager;
    public ScriptableAudios audioListSO;
    public AudioMixerSnapshot AudioSnapGame;
    public AudioMixerSnapshot AudioSnapMenu;
    public AudioMixer am;
    private void Awake()
    {
        if (AudioManager != null)
            Destroy(this.gameObject);
        AudioManager = this;

    }
    public void OnMenu()
    {
        AudioSnapMenu.TransitionTo(0.1f);
    }
    public void OnGame()
    {
        AudioSnapGame.TransitionTo(0.1f);
    }

}
