using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnClickCube : MonoBehaviour
{
    private AudioSource _audioSource;
    void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }
    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            var mousep = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousep.z -= 10f;
            //var ray = Physics.Raycast(mousep, new Vector3(0, 0, 1f));
            RaycastHit hit;
            //Debug.DrawLine(mousep, new Vector3(0, 0, 10f));
            if (Physics.Raycast(mousep, new Vector3(0, 0, 1f), out hit, Mathf.Infinity))
            {
                _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.soundEffectPing;
                _audioSource.Play();
            }
        }
        if(Input.GetKey("space"))
        {
            _audioSource.clip = AudioManagerClips.AudioManager.audioListSO.soundEffectBoing;
            _audioSource.Play();
        }

    }
}
