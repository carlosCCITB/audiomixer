using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Sounds", menuName = "ScriptableObjects/Sounds", order = 1)]
public class ScriptableAudios : ScriptableObject
{
    public AudioClip music;
    public AudioClip soundEffectBoing;
    public AudioClip soundEffectPing;
}
